#!/usr/bin/env python
# coding: utf-8

# # Taiwan Weight Stock Index Analysis - Knightbearr
# #By- Aarush Kumar
# #Dated: August 29,2021

# In[1]:


import numpy as np 
import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sns
plt.style.use('fivethirtyeight')
pd.set_option('display.width', 100)
pd.set_option('display.max_rows', 25)
pd.set_option('display.max_columns', 25)
get_ipython().run_line_magic('matplotlib', 'inline')
print('='*100)
print('Ready To Launch !!!!')
print('='*100)


# In[2]:


# Load data
df = pd.read_csv('/home/aarush100616/Downloads/Projects/Taiwan Weight Stock Index Analysis/TWII.csv')
df


# ## Checking Data

# In[3]:


# Getting information
df.info()


# In[4]:


# Checking the null value
df.isna().mean().to_frame()


# In[5]:


# Droping missing value
df = df.dropna()


# In[6]:


# Checking missing value
df.isna().mean().to_frame()


# In[7]:


# Check the data
df


# In[8]:


# Convert Date Columns to Timestamp
df['Date'] = pd.to_datetime(df['Date'])


# In[9]:


# Check type
type(df['Date'][0])


# In[10]:


# Set the Date column to index
df.set_index('Date',inplace=True)

# Checking the first 5 rows of data
df.head()


# In[11]:


# Checking the last 5 rows of data
df.tail()


# ## Analysis on Open, High, Low, and Close from 2001 - Current Year

# In[12]:


# Accomodate data without Volume
data = df.drop('Volume',axis=1)


# In[13]:


# Plotting Data
plt.figure(figsize=(20,10))
sns.lineplot(data=data.loc['2001-01-04':'2001-12-31'])
plt.title('Taiwan Stocks in 2001 (Jan - Dec)')
plt.show()


# In[14]:


# Plotting Data
plt.figure(figsize=(20,10))
sns.lineplot(data=data.loc['2021-01-01':'2021-08-27'])
plt.title('Taiwan Stocks in 2021 (Jan - Agst)')
plt.show()


# ## Taiwan Stocks From 2002 - 2020

# In[15]:


# Make a For Loop and Plot the Data
for year in range(2002,2020+1):
    plt.figure(figsize=(20,10))
    sns.lineplot(data=data.loc[f'{year}-01-01':f'{year}-12-31'])
    plt.title(f'Taiwan Stocks in {year}')
    plt.show()
    print('\n')


# ## Volume Analysis From 2001 - Current Year

# In[16]:


# Accomodate Data Volume
volume = df['Volume']


# In[17]:


# Plotting Data
plt.figure(figsize=(20,10))
sns.lineplot(data=volume.loc['2001-01-04':'2001-12-31'])
plt.title('Taiwan Stocks Volume in 2001 (Jan - Dec)')
plt.show()


# In[18]:


# Plotting Data
plt.figure(figsize=(20,10))
sns.lineplot(data=volume.loc['2021-01-01':'2021-08-28'])
plt.title('Taiwan Stocks Volume in 2001 (Jan - Dec)')
plt.show()


# ## Taiwan Volume Stocks From 2002 - 2020

# In[19]:


# Make a For loop and Plot the Data
for year in range(2002,2020+1):
    plt.figure(figsize=(20,10))
    sns.lineplot(data=volume.loc[f'{year}-01-01':f'{year}-12-31'],
                 linewidth=1)
    plt.title(f'Taiwan Stock Volume in {year}')
    plt.show()
    print('\n')


# ## Overall Analysis Taiwan Weight Stock Index

# In[20]:


# Plotting Data
plt.figure(figsize=(20,10))
sns.lineplot(data=data,linewidth=1)
plt.title(f'Taiwan Overall Stocks(2001-2021)')
plt.show()


# In[21]:


# Plotting Data
plt.figure(figsize=(20,10))
sns.lineplot(data=volume,linewidth=1)
plt.title(f'Taiwan Overall Volume Stocks(2001-2021)')
plt.show()


# ## Time Series Analysis on Open, Closing, and Volume Stock

# In[22]:


# Accomodate Data
open_stock = df[['Open']]
close_stock = df[['Close']]
volume_stock = df[['Volume']]


# In[23]:


# Make A Plot Function
def plot_timeseries(tdf,label1,label2,col,title):
    plt.figure(figsize=(25,15))
    sns.lineplot(data=tdf,
                 label=label1,
                 legend=False,)
    sns.lineplot(data=df[col],
                 label=label2,
                 legend=False,)
    plt.title(title)
    plt.legend()
    plt.show()


# ## Simple Moving Average

# In[25]:


# Prepare Data
simple_moving_average_close_stock = close_stock.rolling(window=30).mean()

# Plotting Data
plot_timeseries(simple_moving_average_close_stock, 
                'Moving Average',
                'Actual',
                'Close',
                'Close Stock Moving Average')


# In[26]:


# Prepare Data
simple_moving_average_volume_stock = volume_stock.rolling(window=30).mean()

# Plotting Data
plot_timeseries(simple_moving_average_volume_stock,
                'Moving Average',
                'Actual',
                'Volume',
                'Volume Stock Moving Average')


# ## Weighted Moving Average

# In[27]:


# Prepare Data
weights = np.arange(1,31)
open_MV = df['Open'].rolling(30).apply(lambda close: np.dot(close, weights)/weights.sum(), 
                                      raw=True)

# Plotting Data
plot_timeseries(open_MV,
                'Weighted Moving Average',
                'Actual',
                'Open',
                'Open Stock Weighted Moving Average')


# In[28]:


# Prepare Data
weights = np.arange(1,31)
close_MV = df['Close'].rolling(30).apply(lambda close: np.dot(close, weights)/weights.sum(), 
                                        raw=True)

# Plotting Data
plot_timeseries(close_MV,
                'Weighted Moving Average',
                'Actual',
                'Close',
                'Closing Stock Weighted Moving Average')


# In[29]:


# Prepare Data
weights = np.arange(1,31)
volume_MV = df['Volume'].rolling(30).apply(lambda close: np.dot(close, weights)/weights.sum(), 
                                          raw=True)

# Plotting Data
plot_timeseries(volume_MV,
                'Weighted Moving Average',
                'Actual',
                'Volume',
                'Closing Stock Weighted Moving Average')


# ## Exponential Moving Average

# In[30]:


# Prepare Data
expo_mv_open = df['Open'].ewm(span=30, 
                              adjust=False).mean()

# Plotting Data
plot_timeseries(expo_mv_open,
                'Exponential Moving Average',
                'Actual',
                'Open',
                'Open Stock Exponential Moving Average')


# In[31]:


# Prepare Data
expo_mv_close = df['Close'].ewm(span=30, 
                                adjust=False).mean()

# Plotting Data
plot_timeseries(expo_mv_close,
                'Exponential Moving Average',
                'Actual',
                'Close',
                'Close Stock Exponential Moving Average')


# In[32]:


# Prepare Data
expo_mv_volume = df['Volume'].ewm(span=30, 
                                  adjust=False).mean()

# Plotting Data
plot_timeseries(expo_mv_volume,
                'Exponential Moving Average',
                'Actual',
                'Volume',
                'Volume Stock Exponential Moving Average')

